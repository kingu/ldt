
var tarot = {
    gameId: false,
    gamedata: {
	userName: false,
	userRace: false,
	userClass: false,
	gamePhase: "Init",
	userDeck: [],
	deck: [],
	hand: [],
	discard: [],
	trash: [],
      	npc: {}
    },

    init: function() {
	var gd = JSON.parse(localStorage.getItem(conf.prefix + "_" + this.gameId + "_gamedata"));
	this.gamedata = Object.assign(this.gamedata, gd);
	if (this.gamedata.gamePhase == "Init") {
	    // Here we should check for cards with xp to be pre-equipped by user.
            this.gamedata.deck = Object.keys(this.gamedata.userDeck);
	    utils.shuffleArray(this.gamedata.deck);	
	    this.fillHand();
	    this.gamedata.gamePhase = "pc_turn";
	}
	this.displayHand();
	this.proceed();
    },
    proceed: function() {
	if (this.gamedata.gamePhase == "pc_turn") {
	    // make hand clickable
	    console.log(this.gamedata);
	}
    },
    fillHand: function() {
	while (this.gamedata.hand.length < 6) {
	    this.gamedata.hand.push(this.drawCard(this.gamedata.deck));
	}
    },
    drawCard: function (deck) { return deck.shift(); },
    clickOnHand: function (cardNum) {
	console.log(this.gamedata.hand[cardNum]);
    },
    displayCard: function(id, card) {
	var image =  "images/deck/" + card.Rank + card.Color + ".jpg";	
	//var Hcard = "<div id='" + card.Color + card.Rank + "' class='cardHeader'>";
	var pap = Number.isFinite(card.AP) ? card.AP : "--";
	
	var Hcard = `
<div id='${card.Color}${card.Rank}' class='cardHeader'>
 <div class="cardRankColor">
  <span class="cardRank">${card.Rank}</span>
  <span class="cardColor"><img src='images/${card.Color}.png'/></span>
 </div>
 <span class="cardName">${card.Name}</span>
</div>
<div><img width='120' src='${image}'/></div>	
<div style='width:100%;'>
   <table style='margin:0 auto'>
     <tr style='font-size:10px;'>
       <th>CHI</th><th>AP</th><th>EXP</th><th>SP</th>
     </tr>
     <tr>
      <td>${card.CHI}</td>
      <td>${pap}</td>
      <td>${card.EXP}</td>
      <td>${card.SP}</td></tr>
   </table>
</div>`;
	document.getElementById(id).innerHTML=Hcard;

    },
    
    displayHand: function() {
	for (let i = 0; i < 6; i++) {
	    this.displayCard("ph0"+i,this.gamedata.userDeck[this.gamedata.hand[i]]);
	    document.getElementById("ph0"+i).onclick = function () { tarot.clickOnHand(i) };   
	} 	    
    },
    
    read: function(key) {
	return localStorage.getItem(this.prefix + key);
    }

}

