var utils = {
    
    shuffleArray: function (array) {
	for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
	}
    },
    getParams: function () {
	var result = {};
	var tmp = [];
	location.search
            .substr (1)
            .split ("&")
            .forEach (function (item) {
		tmp = item.split ("=");
		result [tmp[0]] = decodeURIComponent (tmp[1]);
	    });
	return result;
    }
}
